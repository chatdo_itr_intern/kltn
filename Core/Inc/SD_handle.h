/*
 * SD_handle.h
 *
 *  Created on: Jun 15, 2023
 *      Author: N.CHAT
 */

#ifndef INC_SD_HANDLE_H_
#define INC_SD_HANDLE_H_
#include "main.h"
#include "inc/ush_file.h"
#include "ff.h"
static FIL fil,fil2;
static FILINFO Finfo;

int SD_mount();
void File_infor(struct ush_object *self);
int scan_files(char *path );
void get_current_dir(struct ush_object *self);
void write_file(char *path, char *str);
void read_file (char *path);
#endif /* INC_SD_HANDLE_H_ */
