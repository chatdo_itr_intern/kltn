/*

*/

#ifndef USH_CONST_H
#define USH_CONST_H

#ifdef __cplusplus
extern "C" {
#endif

/** Library name used in welcome message. */
#define USH_NAME        "Shell"
/** Library version used to identification purposes. */
#define USH_VERSION     "1.0.0"

#ifdef __cplusplus
}
#endif

#endif /* USH_CONST_H */
