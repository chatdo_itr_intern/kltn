/*

*/

#ifndef USH_FILE_H
#define USH_FILE_H

#ifdef __cplusplus
extern "C" {
#endif

#include "ush_types.h"

/**
 * @brief Search file by name.
 * 
 * Function searches file with given name relative to the current working path.
 * First it is looks for file in commands global namespaces.
 * Next it is looks for file in current working path.
 * Name could be absolute or relative.
 * 
 * @param self - pointer to master ush object
 * @param name - pointer to file name to search
 * 
 * @return pointer to ush file descriptor when successfull, otherwise NULL
 */
struct ush_file_descriptor const* ush_file_find_by_name(struct ush_object *self, const char *name);
#ifdef __cplusplus
}
#endif

#endif /* USH_FILE_H */
