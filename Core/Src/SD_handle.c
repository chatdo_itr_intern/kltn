/*
 * SD_handle.c
 *
 *  Created on: Jun 15, 2023
 *      Author: N.CHAT
 */
#include "SD_handle.h"
#include "main.h"
#include <string.h>
FRESULT res;
FATFS FatFs;
BYTE buff[512]; /* File copy buffer */
UINT cnt = sizeof buff;
FRESULT res;
UINT br, bw; /* File read/write count */
TCHAR ptr[256];
int SD_mount()
{
	if ((res = f_mount(&FatFs, "", 1)) != FR_OK)
	{
		printf("mount error !\r\n");
		return 1;
	}
		return 0;
}

int SD_unmount()
{
	if ((res = f_mount(0, "", 0)) != FR_OK)
	{
		printf("unmount error !\r\n");
		return 1;
	}
	return 0;
}
void ff_wait()
{
	ff_req_grant(&FatFs.sobj);
	return;
}

void ff_release()
{
	ff_rel_grant(&FatFs.sobj);
	return;
}
// void File_infor(struct ush_object *self)
//{
//	DIR Dir; /* Directory object */
//	FILINFO fi_inf;
//	FRESULT res;
//	char ptr[256];
//	char dir_name[20];
//	strcpy(ptr, "/root");
//	// res = f_getcwd(ptr, 256);
//	UINT i;
//	//	res = scan_files(ptr);
//	if (res = f_opendir(&Dir, ptr) != FR_OK)
//	{
//		printf("Can't ofen dir %d \r\n", res);
//		//	  	  f_closedir(&Dir);
//		return;
//	}
//	else
//		printf("open successfully \r\n");
//	// res = scan_files(ptr);
//	res = f_readdir(&Dir, &fi_inf);
//	if (res != FR_OK)
//	{
//		printf("Can't read dir %d \r\n", res);
//		return;
//	}
//	else
//	{
//		strcpy(dir_name, fi_inf.fname);
//		i = strlen(ptr);
//		sprintf(&ptr[i], "/%s", fi_inf.fname);
//		ptr[i] = 0;
//		//		res = f_getcwd(ptr, 256);
//		//		self->curr_dir = ptr;
//		f_chdir(ptr);
//		res = f_getcwd(ptr, 256);
////		self->curr_dir = ptr;
//
//		printf("read successfully \r\n");
//	}
//	f_closedir(&Dir);
//	return;
//}
// int scan_files(
//	char *path /* Start node to be scanned (***also used as work area***) */
//)
//{
//	FRESULT res;
//	DIR dir;
//	UINT i;
//	FILINFO fno;
//
//	res = f_opendir(&dir, path); /* Open the directory */
//	if (res == FR_OK)
//	{
//		for (;;)
//		{
//			res = f_readdir(&dir, &fno); /* Read a directory item */
//			if (res != FR_OK || fno.fname[0] == 0)
//				break; /* Break on error or end of dir */
//			if (fno.fattrib & AM_DIR)
//			{ /* It is a directory */
//				i = strlen(path);
//				sprintf(&path[i], "/%s", fno.fname);
//				res = scan_files(path); /* Enter the directory */
//				if (res != FR_OK)
//					break;
//				path[i] = 0;
//			}
//			else
//			{ /* It is a file. */
//				printf("%s/%s\n", path, fno.fname);
//			}
//		}
//		f_closedir(&dir);
//	}
//
//	return res;
//}
void get_current_dir(struct ush_object *self)
{
	res = f_getcwd(ptr, 256);
	self->curr_dir = ptr;
	return;
}
void create_file(char *path)
{
	if ((res = f_open(&fil, path, FA_OPEN_ALWAYS | FA_WRITE | FA_READ)) != FR_OK)
	{
		printf("touch command error !\r\n ");
		return;
	}
	if ((res = f_close(&fil)) != FR_OK)
	{
		printf("close error res !\r\n");
		return;
	}
}
void write_file(char *path, char *str)
{
	if ((res = f_open(&fil, path, FA_OPEN_APPEND | FA_WRITE | FA_READ)) != FR_OK)
	{
		printf("echo command error ! \r\n ");
		return;
	}
	if ((res = f_write(&fil, str, strlen(str), &bw)) != FR_OK)
	{
		printf("write file error \r\n ");
		return;
	}
	if ((res = f_close(&fil)) != FR_OK)
	{
		printf("close error res \r\n");
		return;
	}
}

void read_file(char *path)
{
	if ((res = f_open(&fil, path, FA_READ)) != FR_OK)
	{
		printf("cat command error !\r\n");
		return;
	}
	for (;;)
	{
		if ((res = f_read(&fil, buff, cnt, &br)) != FR_OK) /* Read a chunk of data from the source file */
		{
			printf("cat command error !%d\r\n", res);
			return;
		}

		if (br != cnt)
		{
			printf("%s", buff);
			break;
		}
		else
			printf("%s", buff);
	}
	f_close(&fil);
	printf("\r\n");
	memset(buff, 0, cnt);
	return;
}

uint8_t read_file_line(char *path)
{
	uint8_t couter_line = 0;
	if ((res = f_open(&fil, path, FA_READ)) != FR_OK)
	{
		printf("cat command error !\r\n");
		return 0;
	}

	while(!f_eof(&fil))
	{
		TCHAR *result = f_gets( buff, cnt,&fil);
		if (result != NULL)
		{
			couter_line ++;
		        }
//		else
//		        {
//			couter_line --;
//		        }

	}
	f_close(&fil);
	memset(buff, 0, cnt);
	return couter_line;
}
uint8_t read_file_head(char *path, uint8_t number_of_line)
{
	uint8_t couter_line = 0;
	if ((res = f_open(&fil, path, FA_READ)) != FR_OK)
	{
		printf("cat command error !\r\n");
		return 0;
	}

	while((!f_eof(&fil)) && (couter_line < number_of_line))
	{
		memset(buff, 0, cnt);
		TCHAR *result = f_gets( buff, cnt,&fil);
		if (result != NULL)
		{
			couter_line ++;
			printf("%s\r", buff);
		}
//		else
//		        {
//			couter_line --;
//		        }

	}
	printf("\r\n");
	f_close(&fil);
	memset(buff, 0, cnt);
	return couter_line;
}

uint8_t read_file_tail(char *path, uint8_t number_of_line)
{
	uint8_t couter_line = 0;
	uint8_t start_read_line;
	uint8_t wc_cmd_resulf = read_file_line(path);
	if (wc_cmd_resulf > number_of_line)
		start_read_line = wc_cmd_resulf - number_of_line ;
	else
		start_read_line = 0;

	if ((res = f_open(&fil, path, FA_READ)) != FR_OK)
	{
		printf("cat command error !\r\n");
		return 0;
	}
	while(!f_eof(&fil))
	{
		memset(buff, 0, cnt);
		TCHAR *result = f_gets( buff, cnt,&fil);
		if (couter_line < start_read_line)
			{
			if (result != NULL)
					{
						couter_line ++;
					}
			}
		else
		if (result != NULL)
		{
			couter_line ++;
			printf("%s\r", result);
		}
	}
	printf("\r\n");
	f_close(&fil);
	memset(buff, 0, cnt);
	return couter_line;
}
