/* USER CODE BEGIN Header */
/*
 * main.c
 *
 *  Created on: May 15, 2023
 *      Author: N.CHAT
 */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"
#include "fatfs.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "inc/ush.h"
#include <stdio.h>
#include <string.h>
#include "SD_handle.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define SHELL_WORK_BUFFER_SIZE 256
#define SHELL_HOSTNAME_SIZE 16

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
SPI_HandleTypeDef hspi1;

UART_HandleTypeDef huart2;

osThreadId ush_taskHandle;
/* USER CODE BEGIN PV */
char tx_data[30] = "\rWELCOME MICROSHELL \n\r"; /* tx data string to active uart callback*/
char rx_data;                                   /* receive data */
uint8_t flag = 1;                               /* variable to detect when enter a char from terminal*/
bool ush_1 = false;
bool ush_2 = false;

static char g_hostname_data[SHELL_HOSTNAME_SIZE + 1];
static char g_input_buffer[SHELL_WORK_BUFFER_SIZE];
static char g_output_buffer[SHELL_WORK_BUFFER_SIZE];
static struct ush_object g_ush; /*shell object struct*/
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_SPI1_Init(void);
void start_ush_task(void const * argument);

/* USER CODE BEGIN PFP */
void shell_init(void); /*funtion to init shell*/
static int write_char(struct ush_object *self, char ch);
static int read_char(struct ush_object *self, char *ch);

#if defined(__GNUC__) /*define to use printf fuction*/
int _write(int fd, char *ptr, int len)
{
  HAL_UART_Transmit(&huart2, (uint8_t *)ptr, len, HAL_MAX_DELAY);
  return len;
}

#endif
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
xTaskHandle cmd_Task_Hnadler;
xTaskHandle test_Task_Hnadler;
// xTaskHandle SDCARD_Task_Hnadler;
// void Startsdstack(void const * argument)
//{
//	while (1)
//	{
//		printf("hello \r\n");
//		ush_service(&g_ush);
//		vTaskDelay(10);
//	}
//}
void cmd_task()
{
  while (1)
  {
//	  printf("hello \r\n");
    g_ush.file->exec(&g_ush, g_ush.file, g_ush.argc, g_ush.argv);
//        vTaskDelay(10);
//	  vTaskPrioritySet(cmd_Task_Hnadler, uxTaskPriorityGet(ush_taskHandle) - 1);
//    vTaskPrioritySet(cmd_Task_Hnadler, 0);
	  vTaskSuspend(cmd_Task_Hnadler);
	  g_ush.state = USH_STATE_RESET_PROMPT;
//    read_file_2("/abc.txt", "/hi.txt");
//    read_file("abc.txt");

//    write_file("abc.txt", "haizzz");
//    vTaskResume(ush_taskHandle);
//    vTaskDelay(100);
//	  vTaskSuspend(cmd_Task_Hnadler);
//    vTaskDelete(cmd_Task_Hnadler);
//    vTaskResume(cmd_Task_Hnadler);
//    vTaskDelay(100);

  }
}
 void test_task()
{
	while (1)
	{
//		read_file("abc.txt");
		printf("hello \r\n");
		vTaskSuspend(test_Task_Hnadler);
		 vTaskDelete(test_Task_Hnadler);
	}
 }
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART2_UART_Init();
  MX_SPI1_Init();
  MX_FATFS_Init();
  /* USER CODE BEGIN 2 */

  shell_init();
//    reboot();
  HAL_UART_Transmit(&huart2, tx_data, sizeof(tx_data), 100);
  HAL_UART_Receive_IT(&huart2, &rx_data, 1);
  //  xTaskCreate(test_task, "test", 128, NULL, 1, &test_Task_Hnadler);
  //  xTaskCreate(SDCARD_Task, "SD", 128, NULL, 2, &SDCARD_Task_Hnadler);
//  xTaskCreate(cmd_task, "cmd_task", 128, NULL, uxTaskPriorityGet(ush_taskHandle) - 1, &cmd_Task_Hnadler);

  /* USER CODE END 2 */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of ush_task */
  osThreadDef(ush_task, start_ush_task, osPriorityNormal, 0, 128);
  ush_taskHandle = osThreadCreate(osThread(ush_task), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* Start scheduler */
  osKernelStart();
  /* We should never get here as control is now taken by the scheduler */
  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE3);

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 60;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 2;
  RCC_OscInitStruct.PLL.PLLR = 2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief SPI1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI1_Init(void)
{

  /* USER CODE BEGIN SPI1_Init 0 */

  /* USER CODE END SPI1_Init 0 */

  /* USER CODE BEGIN SPI1_Init 1 */

  /* USER CODE END SPI1_Init 1 */
  /* SPI1 parameter configuration*/
  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_MASTER;
  hspi1.Init.Direction = SPI_DIRECTION_2LINES;
  hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi1.Init.NSS = SPI_NSS_SOFT;
  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_8;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI1_Init 2 */

  /* USER CODE END SPI1_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};
/* USER CODE BEGIN MX_GPIO_Init_1 */
/* USER CODE END MX_GPIO_Init_1 */

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_6, GPIO_PIN_RESET);

  /*Configure GPIO pin : B1_Pin */
  GPIO_InitStruct.Pin = B1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(B1_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : PB6 */
  GPIO_InitStruct.Pin = GPIO_PIN_6;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

/* USER CODE BEGIN MX_GPIO_Init_2 */
/* USER CODE END MX_GPIO_Init_2 */
}

/* USER CODE BEGIN 4 */
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
  if (huart->Instance == USART2)
  {
    flag = 1; // This flag indicates received data
    read_char(&g_ush, rx_data);
  }
}

static int write_char(struct ush_object *self, char ch) /*function to write char from shell to uart*/
{
  (void)self;

  HAL_UART_Transmit(&huart2, &ch, sizeof(ch), 50);
  flag = 0; // After transmission, turn off the flag and wait for the next data reception
  return 1;
}

static int read_char(struct ush_object *self, char *ch) /*function to read char from uart to shell*/
{
  (void)self;
  if (flag == 1)
  { // If there is data received from uart, read it
    HAL_UART_Receive_IT(&huart2, &rx_data, sizeof(rx_data));
    *ch = rx_data;
    return 1;
  }
  else
    return 0;
}

const struct ush_io_interface g_ush_io_interface = {
    .read = read_char,
    .write = write_char,
};
static const struct ush_descriptor g_ush_desc = {
    .io = &g_ush_io_interface,
    .input_buffer = g_input_buffer,
    .input_buffer_size = sizeof(g_input_buffer),
    .output_buffer = g_output_buffer,
    .output_buffer_size = sizeof(g_output_buffer),
    .path_max_length = SHELL_WORK_BUFFER_SIZE,
    .hostname = g_hostname_data,
};
void shell_init(void)
{
  if (SD_mount())
	    USH_ASSERT(false);
  strcpy(g_hostname_data, "FETEL");
  ush_init(&g_ush, &g_ush_desc);

}
char *shell_get_hostname(void)
{
  return g_hostname_data;
}

void shell_set_hostname(const char *hostname)
{
  strncpy(g_hostname_data, hostname, sizeof(g_hostname_data));
  g_hostname_data[sizeof(g_hostname_data) - 1] = 0;
}
void create_task()
{
//	vTaskResume(cmd_Task_Hnadler);
//	vTaskPrioritySet(cmd_Task_Hnadler, uxTaskPriorityGet(ush_taskHandle) + 1);
//	vTaskDelete(cmd_Task_Hnadler);
	if (ush_1 != true)
  xTaskCreate(cmd_task, "cmd_task", 128, NULL, uxTaskPriorityGet(ush_taskHandle) + 1, &cmd_Task_Hnadler);
	else
	{
//		vTaskPrioritySet(cmd_Task_Hnadler, uxTaskPriorityGet(ush_taskHandle) + 1);
		vTaskResume(cmd_Task_Hnadler);}
		ush_1 = true;
//
//
//
//		if (ush_2 != true)
//			xTaskCreate(test_task, "test_task", 128, NULL, uxTaskPriorityGet(ush_taskHandle) + 1, &test_Task_Hnadler);
//		else
//			vTaskResume(test_Task_Hnadler);
//			ush_2 = true;
//  vTaskSuspend(ush_taskHandle);
}
/* USER CODE END 4 */

/* USER CODE BEGIN Header_start_ush_task */
/**
 * @brief  Function implementing the ush_task thread.
 * @param  argument: Not used
 * @retval None
 */
/* USER CODE END Header_start_ush_task */
void start_ush_task(void const * argument)
{
  /* USER CODE BEGIN 5 */
  /* Infinite loop */
  for (;;)
  {
    ush_service(&g_ush);
  }
  /* USER CODE END 5 */
}

/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM6 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM6) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
