/*
 * reboot.c
 *
 *  Created on: May 15, 2023
 *      Author: N.CHAT
 */
#include <string.h>
#include <stdio.h>
#include "ff.h"
#include "diskio.h"
#include <stdio.h>
#include <stdlib.h>
#include "stdint.h"
#include <time.h>

int reboot()
{
	printf("hello bug \n\r");
	FATFS FatFs; /* Filesystem object for logical drive */
	FIL fil;
	FRESULT res;
	UINT bw;
	DIR dir;
	UINT br;
	char buffer[512];
	char str[16];
	BYTE work[_MAX_SS]; // Formats the drive if it has yet to be formatted

	//	if (res = f_mkfs("0:", FM_FAT32, 0, work, sizeof work) != FR_OK)
	//		{
	//			printf("format error res %d\n", res);
	//			return 1;
	//		}
	/* mount SD card-----------------------------------------------------------------------------------------------------------*/
	if ((res = f_mount(&FatFs, "", 1)) != FR_OK)
	{
		printf("mount error res %d\n", res);
		return 1;
	}
	/*Create / -----------------------------------------------------------------------------------------------------------------*/
	if ((res = f_mkdir("root")) != FR_OK)
	{
		printf("f_mkdir error res root %d\n", res);
		return 1;
	}
	/*root/proc---------------------------------------------------------------------------------------------------------------------*/
	if ((res = f_mkdir("root/proc")) != FR_OK)
	{
		printf("f_mkdir error res proc %d\n", res);
		return 1;
	}
	/*root/bin----------------------------------------------------------------------------------------------------------------------*/
	if ((res = f_mkdir("root/bin")) != FR_OK)
	{
		printf("f_mkdir error res %d\n", res);
		return 1;
	}
	/*root/home----------------------------------------------------------------------------------------------------------------------*/
	if ((res = f_mkdir("root/home")) != FR_OK)
	{
		printf("f_mkdir error res %d\n", res);
		return 1;
	}
	/*root/boot----------------------------------------------------------------------------------------------------------------------*/
	if ((res = f_mkdir("root/bot")) != FR_OK)
	{
		printf("f_mkdir error res %d\n", res);
		return 1;
	}

	/*root/lib----------------------------------------------------------------------------------------------------------------------*/
	if ((res = f_mkdir("root/lib")) != FR_OK)
	{
		printf("f_mkdir error res %d\n", res);
		return 1;
	}
	/*root/home/fetel----------------------------------------------------------------------------------------------------------------------*/
	if ((res = f_mkdir("root/home/fetel")) != FR_OK)
	{
		printf("f_mkdir error res %d\n", res);
		return 1;
	}
	/* root/home/fetel/sv.txt -----------------------------------------------------------------------------*/
	if ((res = f_open(&fil, "root/home/fetel/sv.txt", FA_OPEN_ALWAYS | FA_WRITE | FA_READ)) != FR_OK)
	{
		printf("open error res %d\n", res);
		return 1;
	}

	for (int i = 0; i < 10; i++)
	{
		if ((res = f_lseek(&fil, f_size(&fil))))
		{
			printf("lseek error res %d\n", res);
			return 1;
		}
		char ch[15] = "sucessfull \n\r";
		if ((res = f_write(&fil, ch, sizeof ch, &bw)) != FR_OK)
		{
			printf("write error res %d\n", res);
			return 1;
		}
	}

	memset(buffer, NULL, sizeof(buffer));
	if ((res = f_read(&fil, *buffer, sizeof buffer, &br)) != FR_OK)
	{
		printf("read error! res %d\n", res);
		return 1;
	}
	if ((res = f_close(&fil)) != FR_OK)
	{
		printf("close error res %d\n", res);
		return 1;
	}
	/* root/home/fetel/readme.txt -----------------------------------------------------------------------------*/
	if ((res = f_open(&fil, "root/home/fetel/readme.txt", FA_OPEN_ALWAYS | FA_WRITE | FA_READ)) != FR_OK)
	{
		printf("open error res %d\n", res);
		return 1;
	}

	if ((res = f_lseek(&fil, f_size(&fil))))
	{
		printf("lseek error res %d\n", res);
		return 1;
	}
	char ch[100] = "Welcome fetel communication !!!";

	if ((res = f_write(&fil, ch, sizeof ch, &bw)) != FR_OK)
	{
		printf("write error res %d\n", res);
		return 1;
	}
	if ((res = f_close(&fil)) != FR_OK)
	{
		printf("close error res %d\n", res);
		return 1;
	}

	printf("done");
	return 0;
}
/*Format-------------------------------------------------------------------------------------------------------------------------*/
FRESULT format(void)
{
	DIR dir;

	FATFS FatFs; /* Filesystem object for logical drive */
	FIL fil;
	FRESULT res;
	UINT bw;
	UINT br;
	char buffer[1000];
	char str[16];
	FILINFO fno;
	char *path[1000];
	sprintf(path, "%s", "root");
	res = f_opendir(&dir, "root"); /* Open the directory */
								   // res = f_unlink("root");
	if (res == FR_OK)
	{
		for (;;)
		{
			res = f_readdir(&dir, &fno); /* Read a directory item */
			if (res != FR_OK || fno.fname[0] == 0)
				break;				  /* Break on error or end of dir */
			if (fno.fattrib & AM_DIR) /* It is a directory */
			{
				if (!(strcmp("SYSTEM~1", fno.fname)))
					continue;
				res = f_unlink(fno.fname);
				if (res == FR_DENIED)
					continue;
			}
			else
			{ /* It is a file. */
				res = f_unlink(fno.fname);
			}
		}
		f_closedir(&dir);
	}
	return res;
}
