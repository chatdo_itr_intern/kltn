/*
 * ush_cmd_mkdir.c
 *
 *  Created on: Jun 17, 2023
 *      Author: N.CHAT
 */

#include "inc/ush_types.h"
#include "inc/ush_config.h"

#if USH_CONFIG_ENABLE_FEATURE_COMMANDS == 1

#if USH_CONFIG_ENABLE_COMMAND_HELP == 1
extern void ush_buildin_cmd_help_callback(struct ush_object *self, struct ush_file_descriptor const *file, int argc, char *argv[]);
#endif /* USH_CONFIG_ENABLE_COMMAND_HELP */

#if USH_CONFIG_ENABLE_COMMAND_LS == 1
extern void ush_buildin_cmd_ls_callback(struct ush_object *self, struct ush_file_descriptor const *file, int argc, char *argv[]);
#endif /* USH_CONFIG_ENABLE_COMMAND_LS */

#if USH_CONFIG_ENABLE_COMMAND_CD == 1
extern void ush_buildin_cmd_cd_callback(struct ush_object *self, struct ush_file_descriptor const *file, int argc, char *argv[]);
#endif /* USH_CONFIG_ENABLE_COMMAND_CD */

#if USH_CONFIG_ENABLE_COMMAND_PWD == 1
extern void ush_buildin_cmd_pwd_callback(struct ush_object *self, struct ush_file_descriptor const *file, int argc, char *argv[]);
#endif /* USH_CONFIG_ENABLE_COMMAND_PWD */

#if USH_CONFIG_ENABLE_COMMAND_CAT == 1
extern void ush_buildin_cmd_cat_callback(struct ush_object *self, struct ush_file_descriptor const *file, int argc, char *argv[]);
// extern void ush_buildin_cmd_cat_service(struct ush_object *self, struct ush_file_descriptor const *file);
#endif /* USH_CONFIG_ENABLE_COMMAND_CAT */

#if USH_CONFIG_ENABLE_COMMAND_XXD == 1
extern void ush_buildin_cmd_xxd_callback(struct ush_object *self, struct ush_file_descriptor const *file, int argc, char *argv[]);
extern void ush_buildin_cmd_xxd_service(struct ush_object *self, struct ush_file_descriptor const *file);
#endif /* USH_CONFIG_ENABLE_COMMAND_XXD */

#if USH_CONFIG_ENABLE_COMMAND_ECHO == 1
extern void ush_buildin_cmd_echo_callback(struct ush_object *self, struct ush_file_descriptor const *file, int argc, char *argv[]);
#endif /* USH_CONFIG_ENABLE_COMMAND_ECHO */

extern void ush_buildin_cmd_puts_callback(struct ush_object *self, struct ush_file_descriptor const *file, int argc, char *argv[]);

extern void ush_buildin_cmd_mkdir_callback(struct ush_object *self, struct ush_file_descriptor const *file, int argc, char *argv[]);

extern void ush_buildin_cmd_touch_callback(struct ush_object *self, struct ush_file_descriptor const *file, int argc, char *argv[]);

extern void ush_buildin_cmd_mv_callback(struct ush_object *self, struct ush_file_descriptor const *file, int argc, char *argv[]);

extern void ush_buildin_cmd_cp_callback(struct ush_object *self, struct ush_file_descriptor const *file, int argc, char *argv[]);

extern void ush_buildin_cmd_rm_callback(struct ush_object *self, struct ush_file_descriptor const *file, int argc, char *argv[]);

extern void ush_buildin_cmd_wc_callback(struct ush_object *self, struct ush_file_descriptor const *file, int argc, char *argv[]);

extern void ush_buildin_cmd_head_callback(struct ush_object *self, struct ush_file_descriptor const *file, int argc, char *argv[]);

extern void ush_buildin_cmd_tail_callback(struct ush_object *self, struct ush_file_descriptor const *file, int argc, char *argv[]);

extern void reset_exec_callback(struct ush_object *self, struct ush_file_descriptor const *file, int argc, char *argv[]);

const struct ush_file_descriptor g_ush_buildin_commands[] = {

#if USH_CONFIG_ENABLE_COMMAND_HELP == 1
    {
        .name = "help",
        .description = "list available commands",
        .help = "usage: help [file]\r\n",
        .exec = ush_buildin_cmd_help_callback,
        //        .process = ush_buildin_cmd_help_service,
    },
#endif /* USH_CONFIG_ENABLE_COMMAND_HELP */
#if USH_CONFIG_ENABLE_COMMAND_LS == 1
    {
        .name = "ls",
        .description = "list directory content",
        .help = "usage: ls [path]\r\n",
        .exec = ush_buildin_cmd_ls_callback,
        //        .process = ush_buildin_cmd_ls_service,
    },
#endif /* USH_CONFIG_ENABLE_COMMAND_LS */
#if USH_CONFIG_ENABLE_COMMAND_CD == 1
    {
        .name = "cd",
        .description = "change current directory",
        .help = "usage: cd <path>\r\n",
        .exec = ush_buildin_cmd_cd_callback,
    },
#endif /* USH_CONFIG_ENABLE_COMMAND_CD */
#if USH_CONFIG_ENABLE_COMMAND_PWD == 1
    {
        .name = "pwd",
        .description = "print current directory",
        .help = "usage: pwd\r\n",
        .exec = ush_buildin_cmd_pwd_callback,
    },
#endif /* USH_CONFIG_ENABLE_COMMAND_PWD */
#if USH_CONFIG_ENABLE_COMMAND_CAT == 1
    {
        .name = "cat",
        .description = "print files content",
        .help = "usage: cat <files...>\r\n",
        .exec = ush_buildin_cmd_cat_callback,
        //        .process = ush_buildin_cmd_cat_service,
    },
#endif /* USH_CONFIG_ENABLE_COMMAND_CAT */
#if USH_CONFIG_ENABLE_COMMAND_XXD == 1
    {
        .name = "xxd",
        .description = "dump file hex content",
        .help = "usage: xxd <file>\r\n",
        .exec = ush_buildin_cmd_xxd_callback,
        .process = ush_buildin_cmd_xxd_service,
    },
#endif /* USH_CONFIG_ENABLE_COMMAND_XXD */
#if USH_CONFIG_ENABLE_COMMAND_ECHO == 1
    {
        .name = "echo",
        .description = "print string to file",
        .help = "usage: echo <string> > <file>\r\n"
                "       echo [string]\r\n"
                "       * can be ascii-hex encoded string with \\xNN format.\r\n",
        .exec = ush_buildin_cmd_echo_callback,
    },
#endif /* USH_CONFIG_ENABLE_COMMAND_ECHO */
       /* Command mkdir*/
    {
        .name = "mkdir",
        .description = "create a directory",
        .help = "usage: mkdir <directory name> \r\n",
        .exec = ush_buildin_cmd_mkdir_callback,
    },
    /* Command touch*/
    {
        .name = "touch",
        .description = "create a file",
        .help = "usage: touch <file name> \r\n ",
        .exec = ush_buildin_cmd_touch_callback,
    },
    /* Command mv*/
    {
        .name = "mv",
        .description = "moves or rename files and directories",
        .help = "usage: mk <old path> <new path>\r\n",
        .exec = ush_buildin_cmd_mv_callback,
    },
    /* Command rm*/
    {
        .name = "rm",
        .description = "removes a file or sub-directory",
        .help = "usage: rm <file name>\r\n",
        .exec = ush_buildin_cmd_rm_callback,
    },
    /* Command reset*/
    {
        .name = "reset",
        .description = "reset shell",
        .exec = reset_exec_callback,
    },
	/* Command wc*/
	    {
	        .name = "wc",
	        .description = "count number of file",
	        .help = "usage: wc <path>\r\n",
	        .exec = ush_buildin_cmd_wc_callback,
	    },
		/* Command puts*/
			    {
			        .name = "puts",
			        .description = "write a string",
			        .help = "usage: puts <strings> <path>\r\n",
			        .exec = ush_buildin_cmd_puts_callback,
			    },
				/* Command head*/
	    {
	    	.name = "head",
			.description = "Display <N> first line of file text at <path>",
	        .help = "usage: head <n> <path>\r\n",
	        .exec = ush_buildin_cmd_head_callback,
	    },

		/* Command tail*/
	    {
	    	.name = "tail",
			.description = "Display <N> last line of file text at <path>",
	        .help = "usage: tail <n> <path>\r\n",
	        .exec = ush_buildin_cmd_tail_callback,
	    },
	/* Command cp*/
	    {
	        .name = "cp",
	        .description = "copy files and directories",
			.help = "usage: cp <path name> <new path>\r\n",
	        .exec = ush_buildin_cmd_cp_callback,
	    },

};

const size_t g_ush_buildin_commands_num = sizeof(g_ush_buildin_commands) / sizeof(g_ush_buildin_commands[0]);

#endif /* USH_CONFIG_ENABLE_FEATURE_COMMANDS */
