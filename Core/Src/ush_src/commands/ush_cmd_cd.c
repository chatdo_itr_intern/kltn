/*
 * ush_cmd_mkdir.c
 *
 *  Created on: Jun 17, 2023
 *      Author: N.CHAT
 */

#include "inc/ush.h"
#include "ff.h"

#if USH_CONFIG_ENABLE_FEATURE_COMMANDS == 1

#if USH_CONFIG_ENABLE_COMMAND_CD == 1

void ush_buildin_cmd_cd_callback(struct ush_object *self, struct ush_file_descriptor const *file, int argc, char *argv[])
{
        (void)file;
        FRESULT res;
        if (argc != 2)
        {
                ush_print_status(self, USH_STATUS_ERROR_COMMAND_WRONG_ARGUMENTS);
                return;
        }
        if ((res = f_chdir(argv[1])) != FR_OK)
        {
                printf("cd command error !\r\n");
        }
        get_current_dir(self);
        self->state = USH_STATE_RESET_PROMPT;
}

#endif /* USH_CONFIG_ENABLE_COMMAND_CD */

#endif /* USH_CONFIG_ENABLE_FEATURE_COMMANDS */
