/*
 * ush_cmd_cp.c
 *
 *  Created on: 3 Jul 2023
 *      Author: Chat Do
 */
#include "inc/ush.h"
#include "inc/ush_internal.h"
#include "ff.h"
#include "SD_handle.h"
#include <string.h>

#if USH_CONFIG_ENABLE_FEATURE_COMMANDS == 1

void ush_buildin_cmd_cp_callback(struct ush_object *self, struct ush_file_descriptor const *file, int argc, char *argv[])
{
	(void)file;
	FRESULT res;
	int i;
	char path[20];
	if (argc < 3)
	{
		ush_print_status(self, USH_STATUS_ERROR_COMMAND_WRONG_ARGUMENTS);
		return;
	}

	res = f_stat(argv[argc - 1], &Finfo);
	if (res != FR_OK )
		{
						printf("cp command error! \r\n");
		}
		else
			for (i = 1; i < argc - 1; i++)
						{
				strcpy(path, argv[argc - 1]);
				strcat(path, "/");
				strcat(path, argv[i]);
				if (res = f_copy (argv[i], path) != FR_OK)
				{
						printf("cp command error! \r\n");
				}

		}

	self->state = USH_STATE_RESET_PROMPT;
}

#endif /* USH_CONFIG_ENABLE_FEATURE_COMMANDS */

