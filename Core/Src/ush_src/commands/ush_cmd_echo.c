/*
 * ush_cmd_mkdir.c
 *
 *  Created on: Jun 17, 2023
 *      Author: N.CHAT
 */

#include "inc/ush.h"
#include "inc/ush_internal.h"
#include "ff.h"
#include <string.h>
#include "SD_handle.h"

#if USH_CONFIG_ENABLE_FEATURE_COMMANDS == 1

#if USH_CONFIG_ENABLE_COMMAND_ECHO == 1

void ush_buildin_cmd_echo_callback(struct ush_object *self, struct ush_file_descriptor const *file, int argc, char *argv[])
{
        (void)argv;
        (void)file;
        FILINFO Finfo;
        FRESULT res;
        if (argc >= 4)
        {
                if (strcmp(argv[argc - 2], ">") == 0)
                {
                        res = f_stat(argv[argc - 1], &Finfo);
                        if (res != FR_OK || (Finfo.fattrib & AM_DIR))
                        {
                                ush_print_status(self, USH_STATUS_ERROR_FILE_NOT_FOUND);
                                return;
                        }
                        if (Finfo.fattrib & AM_RDO)
                        {
                                ush_print_status(self, USH_STATUS_ERROR_FILE_NOT_WRITABLE);
                                return;
                        }
                        for (int i = 1; i < argc - 2; i++)
                        {
                            write_file(argv[argc - 1], argv[i]);
                                if (i < (argc - 2))
                                        write_file(argv[argc - 1], " ");

                        }
                }
                else
                {
                        for (int i = 1; i < argc; i++)
                        {
                                printf("%s ", argv[i]);
                                if (i == (argc - 1))
                                        printf("\r\n");
                        }
                }
        }
        else if (argc >= 2)
                printf("%s %s\r\n", argv[1], argv[2]);
        else
                printf(" \r\n");

        self->state = USH_STATE_RESET_PROMPT;
}

#endif /* USH_CONFIG_ENABLE_COMMAND_ECHO */

#endif /* USH_CONFIG_ENABLE_FEATURE_COMMANDS */
