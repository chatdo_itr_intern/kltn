/*
 * ush_cmd_head.c
 *
 *  Created on: Jul 4, 2023
 *      Author: Chat Do
 */

#include "inc/ush.h"
#include "inc/ush_internal.h"
#include "ff.h"
#include <string.h>

#if USH_CONFIG_ENABLE_FEATURE_COMMANDS == 1


void ush_buildin_cmd_head_callback(struct ush_object *self, struct ush_file_descriptor const *file, int argc, char *argv[])
{
	(void)file;
	        FILINFO Finfo;
	        FRESULT res;
	        if (argc != 3)
	        {
	                ush_print_status(self, USH_STATUS_ERROR_COMMAND_WRONG_ARGUMENTS);
	                return;
	        }
	                res = f_stat(argv[2], &Finfo);
	                if (res != FR_OK || (Finfo.fattrib & AM_DIR))
	                {
	                        ush_print_status(self, USH_STATUS_ERROR_FILE_NOT_FOUND);
	                        return;
	                }
	                read_file_head(argv[2], atoi(argv[1]));
	        self->state = USH_STATE_RESET_PROMPT;
	        return;
	}



#endif /* USH_CONFIG_ENABLE_FEATURE_COMMANDS */


