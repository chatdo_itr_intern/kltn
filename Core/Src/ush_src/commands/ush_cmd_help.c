/*
 * ush_cmd_mkdir.c
 *
 *  Created on: Jun 17, 2023
 *      Author: N.CHAT
 */

#include "inc/ush.h"
#include "inc/ush_internal.h"

#include <string.h>

#if USH_CONFIG_ENABLE_FEATURE_COMMANDS == 1

#if USH_CONFIG_ENABLE_COMMAND_HELP == 1

void ush_buildin_cmd_help_callback(struct ush_object *self, struct ush_file_descriptor const *file, int argc, char *argv[])
{
        (void)file;

        switch (argc)
        {
        case 1:
                for (size_t i = 0; i < g_ush_buildin_commands_num; i++)
                {
                        printf("%-" USH_STRING(USH_CONFIG_FILENAME_ALIGN_SPACE) "s " USH_SHELL_FONT_COLOR_YELLOW "- %s \r\n" USH_SHELL_FONT_COLOR_WHITE, (char *)g_ush_buildin_commands[i].name, (char *)g_ush_buildin_commands[i].description);
                }
                break;
        case 2:
        {
                struct ush_file_descriptor const *f = ush_file_find_by_name(self, argv[1]);
                if (f == NULL)
                {
                        ush_print_status(self, USH_STATUS_ERROR_FILE_NOT_FOUND);
                        return;
                }
                if (f->help == NULL)
                {
                        ush_print_status(self, USH_STATUS_ERROR_FILE_NO_HELP);
                        return;
                }
                printf("%s", f->help);
                break;
        }
        default:
                ush_print_status(self, USH_STATUS_ERROR_COMMAND_WRONG_ARGUMENTS);
                break;
        }
}

#endif /* USH_CONFIG_ENABLE_COMMAND_HELP */

#endif /* USH_CONFIG_ENABLE_FEATURE_COMMANDS */
