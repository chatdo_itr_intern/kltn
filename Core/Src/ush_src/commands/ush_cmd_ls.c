/*
 * ush_cmd_mkdir.c
 *
 *  Created on: Jun 17, 2023
 *      Author: N.CHAT
 */

#include "inc/ush.h"
#include "inc/ush_internal.h"
#include "ff.h"
#include <string.h>

#if USH_CONFIG_ENABLE_FEATURE_COMMANDS == 1

#if USH_CONFIG_ENABLE_COMMAND_LS == 1

void ush_buildin_cmd_ls_callback(struct ush_object *self, struct ush_file_descriptor const *file, int argc, char *argv[])
{
	(void)file;
		DIR Dir; /* Directory object */
		FILINFO Finfo;
		FRESULT res;
	switch (argc)
	{
	case 1:
		self->path = self->curr_dir;
		break;
	case 2:
	{
		self->path = argv[1];
		break;
	}
	default:
		ush_print_status(self, USH_STATUS_ERROR_COMMAND_WRONG_ARGUMENTS);
		return;
	}

	if ((res = f_opendir(&Dir, self->path)) != FR_OK)
		{
			printf("ls command error ! \r\n");
			self->state = USH_STATE_RESET_PROMPT;
			return;
		}
		for (;;)
		{
			res = f_readdir(&Dir, &Finfo);
			if ((res != FR_OK) || (!Finfo.fname[0]))
				break;
			if (strcmp(Finfo.fname, "SYSTEM~1\0") == 0)
				continue;
//			printf("%c%c%c%c%c   %s \r\n",
//				   (Finfo.fattrib & AM_DIR) ? 'D' : '-',
//				   (Finfo.fattrib & AM_RDO) ? 'R' : '-',
//				   (Finfo.fattrib & AM_HID) ? 'H' : '-',
//				   (Finfo.fattrib & AM_SYS) ? 'S' : '-',
//				   (Finfo.fattrib & AM_ARC) ? 'A' : '-',
//				   Finfo.fname);
			printf("%s \r\n",Finfo.fname);
		}
		f_closedir(&Dir);
		self->state = USH_STATE_RESET_PROMPT;
		return;
}


#endif /* USH_CONFIG_ENABLE_COMMAND_LS */

#endif /* USH_CONFIG_ENABLE_FEATURE_COMMANDS */
