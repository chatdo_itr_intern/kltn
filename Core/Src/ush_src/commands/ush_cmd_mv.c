/*
 * ush_cmd_mv.c
 *
 *  Created on: Jun 17, 2023
 *      Author: N.CHAT
 */

#include "inc/ush.h"
#include "inc/ush_internal.h"
#include "ff.h"
#include "SD_handle.h"
#include <string.h>

#if USH_CONFIG_ENABLE_FEATURE_COMMANDS == 1

void ush_buildin_cmd_mv_callback(struct ush_object *self, struct ush_file_descriptor const *file, int argc, char *argv[])
{
	(void)file;
	FRESULT res;
	int i;
	char path[20];
	if (argc < 3)
	{
		ush_print_status(self, USH_STATUS_ERROR_COMMAND_WRONG_ARGUMENTS);
		return;
	}
	res = f_stat(argv[argc - 1], &Finfo);
	if (res != FR_OK )
	                {
		for (i = 1; i < argc - 1; i++)
			{
				if ((res = f_rename(argv[i], argv[argc - 1])) != FR_OK)
				{
					printf("mv command error! \r\n");
				}
			}
	 }
	else
	{
		for (i = 1; i < argc - 1; i++)
					{
			strcpy(path, argv[argc - 1]);
			strcat(path, "/");
			strcat(path, argv[i]);
			if (res = f_rename(argv[i], path) != FR_OK)
			{
					printf("mv command error! \r\n");
			}
		}

	}

	self->state = USH_STATE_RESET_PROMPT;
}

#endif /* USH_CONFIG_ENABLE_FEATURE_COMMANDS */
