/*
 * ush_cmd_puts.c
 *
 *  Created on: Jul 4, 2023
 *      Author: Chat Do
 */

#include "inc/ush.h"
#include "inc/ush_internal.h"
#include "ff.h"
#include <string.h>
#include "SD_handle.h"

#if USH_CONFIG_ENABLE_FEATURE_COMMANDS == 1

#if USH_CONFIG_ENABLE_COMMAND_ECHO == 1

void ush_buildin_cmd_puts_callback(struct ush_object *self, struct ush_file_descriptor const *file, int argc, char *argv[])
{
        (void)argv;
        (void)file;
        FILINFO Finfo;
        FRESULT res;
        char *buff = (char*) malloc(100 * sizeof(char));
        memset(buff,0,strlen(buff));
        if (argc < 3)
        	{
        		ush_print_status(self, USH_STATUS_ERROR_COMMAND_WRONG_ARGUMENTS);
        		return;
        	}
        res = f_stat(argv[argc - 1], &Finfo);
                                if (res != FR_OK || (Finfo.fattrib & AM_DIR))
                                {
                                        ush_print_status(self, USH_STATUS_ERROR_FILE_NOT_FOUND);
                                        return;
                                }
                                if (Finfo.fattrib & AM_RDO)
                                {
                                        ush_print_status(self, USH_STATUS_ERROR_FILE_NOT_WRITABLE);
                                        return;
                                }
                                for (int i = 1; i < argc - 1; i++)
                                {
                                    strcat(buff, argv[i]);
                                    strcat(buff, " ");
                                }
                                strcat(buff, "\r\n");
                                write_file(argv[argc - 1], buff);
        free(buff);
        memset(buff,0,strlen(buff));
        self->state = USH_STATE_RESET_PROMPT;
}

#endif /* USH_CONFIG_ENABLE_COMMAND_ECHO */

#endif /* USH_CONFIG_ENABLE_FEATURE_COMMANDS */

