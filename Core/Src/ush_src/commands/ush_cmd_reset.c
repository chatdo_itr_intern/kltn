/*
 * ush_cmd_reset.c
 *
 *  Created on: Jun 21, 2023
 *      Author: N.CHAT
 */

#include "inc/ush.h"
void reset_exec_callback(struct ush_object *self, struct ush_file_descriptor const *file, int argc, char *argv[])
{
        (void)file;
        (void)argc;
        (void)argv;

        ush_reset(self);
}
