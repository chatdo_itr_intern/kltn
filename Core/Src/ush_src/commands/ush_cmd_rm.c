/*
 * ush_cmd_rm.c
 *
 *  Created on: Jun 17, 2023
 *      Author: N.CHAT
 */

#include "inc/ush.h"
#include "inc/ush_internal.h"
#include "ff.h"
#include <string.h>

#if USH_CONFIG_ENABLE_FEATURE_COMMANDS == 1

void ush_buildin_cmd_rm_callback(struct ush_object *self, struct ush_file_descriptor const *file, int argc, char *argv[])
{
	(void)file;
	FRESULT res;
	int i;
	if (argc < 2)
	{
		ush_print_status(self, USH_STATUS_ERROR_COMMAND_WRONG_ARGUMENTS);
		return;
	}
	for (i = 1; i < argc; i++)
	{
		if ((res = f_unlink(argv[i])) != FR_OK)
		{
			printf("remove command error! \r\n");
			return;
		}
	}
	self->state = USH_STATE_RESET_PROMPT;
}

#endif /* USH_CONFIG_ENABLE_FEATURE_COMMANDS */
