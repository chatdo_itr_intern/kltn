/*
 * ush_cmd_touch.c
 *
 *  Created on: Jun 17, 2023
 *      Author: N.CHAT
 */
#include "inc/ush.h"
#include "inc/ush_internal.h"
#include "ff.h"
#include <string.h>
#include "SD_handle.h"

#if USH_CONFIG_ENABLE_FEATURE_COMMANDS == 1

void ush_buildin_cmd_touch_callback(struct ush_object *self, struct ush_file_descriptor const *file, int argc, char *argv[])
{
	int i;
	char path[40];
	if (argc < 2)
	{
		ush_print_status(self, USH_STATUS_ERROR_COMMAND_WRONG_ARGUMENTS);
		return;
	}
	for (i = 1; i < argc; i++)
	{
		strcpy(path, self->curr_dir);
		strcat(path, "/");
		strcat(path, argv[i]);
		create_file(path);
	}
	self->state = USH_STATE_RESET_PROMPT;
}

#endif /* USH_CONFIG_ENABLE_FEATURE_COMMANDS */
