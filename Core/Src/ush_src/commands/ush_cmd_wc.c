/*
 * ush_cmd_wc.c
 *
 *  Created on: Jul 4, 2023
 *      Author: Chat Do
 */

#include "inc/ush.h"
#include "inc/ush_internal.h"
#include "ff.h"
#include <string.h>

#if USH_CONFIG_ENABLE_FEATURE_COMMANDS == 1

#if USH_CONFIG_ENABLE_COMMAND_LS == 1

void ush_buildin_cmd_wc_callback(struct ush_object *self, struct ush_file_descriptor const *file, int argc, char *argv[])
{
	(void)file;
	        FILINFO Finfo;
	        FRESULT res;
	        if (argc < 2)
	        {
	                ush_print_status(self, USH_STATUS_ERROR_COMMAND_WRONG_ARGUMENTS);
	                return;
	        }
	        for (int i = 1; i < argc; i++)
	        {
	                res = f_stat(argv[i], &Finfo);
	                if (res != FR_OK || (Finfo.fattrib & AM_DIR))
	                {
	                        ush_print_status(self, USH_STATUS_ERROR_FILE_NOT_FOUND);
	                        return;
	                }
	                printf("%d lines\r\n",read_file_line(argv[i]));
	        }
	        self->state = USH_STATE_RESET_PROMPT;
	        return;
	}


#endif /* USH_CONFIG_ENABLE_COMMAND_LS */

#endif /* USH_CONFIG_ENABLE_FEATURE_COMMANDS */

