/*

*/

#include "inc/ush.h"
#include "inc/ush_internal.h"
#include "inc/ush_shell.h"

#include <stdarg.h>
#include <string.h>
#include "inc/ush_file.h"

void ush_init(struct ush_object *self, const struct ush_descriptor *desc)
{
        USH_ASSERT(self != NULL);
        USH_ASSERT(desc != NULL);

        USH_ASSERT(desc->input_buffer != NULL);
        USH_ASSERT(desc->input_buffer_size > 0);
        USH_ASSERT(desc->output_buffer != NULL);
        USH_ASSERT(desc->output_buffer_size >= 4); /* internal echo buffer */

        USH_ASSERT(desc->io != NULL);
        USH_ASSERT(desc->io->read != NULL);
        USH_ASSERT(desc->io->write != NULL);

        USH_ASSERT(desc->hostname != NULL);

        self->desc = desc;
        get_current_dir(self);
#if USH_CONFIG_ENABLE_FEATURE_COMMANDS == 1
        ush_status_t stat = ush_commands_add(self, &self->buildin_commands, g_ush_buildin_commands, g_ush_buildin_commands_num);
        if (stat != USH_STATUS_OK)
        {
                USH_ASSERT(false);
        }
#endif /* USH_CONFIG_ENABLE_FEATURE_COMMANDS */

        ush_reset(self);
}


bool ush_service(struct ush_object *self)
{
        USH_ASSERT(self != NULL);
        bool busy = false;

        if (ush_reset_service(self) != false)
                return true;
        if (ush_prompt_service(self) != false)
                return true;
        if (ush_read_service(self, &busy) != false)
                return busy;
        if (ush_parse_service(self) != false)
                return true;
        if (ush_write_service(self) != false)
                return true;
        return false;
}

void ush_print_status(struct ush_object *self, ush_status_t status)
{
        USH_ASSERT(self != NULL);
        USH_ASSERT(status < USH_STATUS__TOTAL_NUM);

        char *ret = (char *)ush_utils_get_status_string(status);
        ush_write_pointer(self, ret, USH_STATE_RESET);
}

