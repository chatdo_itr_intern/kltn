/*

*/

#include "inc/ush_file.h"
#include <string.h>

struct ush_file_descriptor const* ush_file_find_by_name(struct ush_object *self, const char *name)
{

        struct ush_node_object *curr;
        struct ush_file_descriptor const *file;

        curr = self->commands;
        while (curr != NULL) {
                for (size_t i = 0; i < curr->file_list_size; i++) {
                        file = &curr->file_list[i];
                        if (strcmp(file->name, name) == 0)
                                return file;
                }
                curr = curr->next;
        }
        return NULL;
}

