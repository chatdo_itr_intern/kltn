################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Core/Src/fs/path/bin.c \
../Core/Src/fs/path/dev.c \
../Core/Src/fs/path/etc.c \
../Core/Src/fs/path/root.c 

OBJS += \
./Core/Src/fs/path/bin.o \
./Core/Src/fs/path/dev.o \
./Core/Src/fs/path/etc.o \
./Core/Src/fs/path/root.o 

C_DEPS += \
./Core/Src/fs/path/bin.d \
./Core/Src/fs/path/dev.d \
./Core/Src/fs/path/etc.d \
./Core/Src/fs/path/root.d 


# Each subdirectory must supply rules for building sources it contributes
Core/Src/fs/path/%.o Core/Src/fs/path/%.su: ../Core/Src/fs/path/%.c Core/Src/fs/path/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -DUSE_HAL_DRIVER -DSTM32F446xx -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -Os -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-Core-2f-Src-2f-fs-2f-path

clean-Core-2f-Src-2f-fs-2f-path:
	-$(RM) ./Core/Src/fs/path/bin.d ./Core/Src/fs/path/bin.o ./Core/Src/fs/path/bin.su ./Core/Src/fs/path/dev.d ./Core/Src/fs/path/dev.o ./Core/Src/fs/path/dev.su ./Core/Src/fs/path/etc.d ./Core/Src/fs/path/etc.o ./Core/Src/fs/path/etc.su ./Core/Src/fs/path/root.d ./Core/Src/fs/path/root.o ./Core/Src/fs/path/root.su

.PHONY: clean-Core-2f-Src-2f-fs-2f-path

