################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Core/Src/fs/fs.c 

OBJS += \
./Core/Src/fs/fs.o 

C_DEPS += \
./Core/Src/fs/fs.d 


# Each subdirectory must supply rules for building sources it contributes
Core/Src/fs/%.o Core/Src/fs/%.su: ../Core/Src/fs/%.c Core/Src/fs/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -DUSE_HAL_DRIVER -DSTM32F446xx -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -Os -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-Core-2f-Src-2f-fs

clean-Core-2f-Src-2f-fs:
	-$(RM) ./Core/Src/fs/fs.d ./Core/Src/fs/fs.o ./Core/Src/fs/fs.su

.PHONY: clean-Core-2f-Src-2f-fs

