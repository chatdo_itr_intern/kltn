################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Core/Src/src/commands/ush_cmd.c \
../Core/Src/src/commands/ush_cmd_cat.c \
../Core/Src/src/commands/ush_cmd_cd.c \
../Core/Src/src/commands/ush_cmd_echo.c \
../Core/Src/src/commands/ush_cmd_help.c \
../Core/Src/src/commands/ush_cmd_ls.c \
../Core/Src/src/commands/ush_cmd_pwd.c \
../Core/Src/src/commands/ush_cmd_xxd.c 

OBJS += \
./Core/Src/src/commands/ush_cmd.o \
./Core/Src/src/commands/ush_cmd_cat.o \
./Core/Src/src/commands/ush_cmd_cd.o \
./Core/Src/src/commands/ush_cmd_echo.o \
./Core/Src/src/commands/ush_cmd_help.o \
./Core/Src/src/commands/ush_cmd_ls.o \
./Core/Src/src/commands/ush_cmd_pwd.o \
./Core/Src/src/commands/ush_cmd_xxd.o 

C_DEPS += \
./Core/Src/src/commands/ush_cmd.d \
./Core/Src/src/commands/ush_cmd_cat.d \
./Core/Src/src/commands/ush_cmd_cd.d \
./Core/Src/src/commands/ush_cmd_echo.d \
./Core/Src/src/commands/ush_cmd_help.d \
./Core/Src/src/commands/ush_cmd_ls.d \
./Core/Src/src/commands/ush_cmd_pwd.d \
./Core/Src/src/commands/ush_cmd_xxd.d 


# Each subdirectory must supply rules for building sources it contributes
Core/Src/src/commands/%.o Core/Src/src/commands/%.su: ../Core/Src/src/commands/%.c Core/Src/src/commands/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -DUSE_HAL_DRIVER -DSTM32F446xx -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -Os -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-Core-2f-Src-2f-src-2f-commands

clean-Core-2f-Src-2f-src-2f-commands:
	-$(RM) ./Core/Src/src/commands/ush_cmd.d ./Core/Src/src/commands/ush_cmd.o ./Core/Src/src/commands/ush_cmd.su ./Core/Src/src/commands/ush_cmd_cat.d ./Core/Src/src/commands/ush_cmd_cat.o ./Core/Src/src/commands/ush_cmd_cat.su ./Core/Src/src/commands/ush_cmd_cd.d ./Core/Src/src/commands/ush_cmd_cd.o ./Core/Src/src/commands/ush_cmd_cd.su ./Core/Src/src/commands/ush_cmd_echo.d ./Core/Src/src/commands/ush_cmd_echo.o ./Core/Src/src/commands/ush_cmd_echo.su ./Core/Src/src/commands/ush_cmd_help.d ./Core/Src/src/commands/ush_cmd_help.o ./Core/Src/src/commands/ush_cmd_help.su ./Core/Src/src/commands/ush_cmd_ls.d ./Core/Src/src/commands/ush_cmd_ls.o ./Core/Src/src/commands/ush_cmd_ls.su ./Core/Src/src/commands/ush_cmd_pwd.d ./Core/Src/src/commands/ush_cmd_pwd.o ./Core/Src/src/commands/ush_cmd_pwd.su ./Core/Src/src/commands/ush_cmd_xxd.d ./Core/Src/src/commands/ush_cmd_xxd.o ./Core/Src/src/commands/ush_cmd_xxd.su

.PHONY: clean-Core-2f-Src-2f-src-2f-commands

