################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Core/Src/src/ush.c \
../Core/Src/src/ush_autocomp.c \
../Core/Src/src/ush_autocomp_state.c \
../Core/Src/src/ush_autocomp_utils.c \
../Core/Src/src/ush_commands.c \
../Core/Src/src/ush_file.c \
../Core/Src/src/ush_node.c \
../Core/Src/src/ush_node_mount.c \
../Core/Src/src/ush_node_utils.c \
../Core/Src/src/ush_parse.c \
../Core/Src/src/ush_parse_char.c \
../Core/Src/src/ush_parse_utils.c \
../Core/Src/src/ush_process.c \
../Core/Src/src/ush_prompt.c \
../Core/Src/src/ush_read.c \
../Core/Src/src/ush_read_char.c \
../Core/Src/src/ush_read_utils.c \
../Core/Src/src/ush_reset.c \
../Core/Src/src/ush_utils.c \
../Core/Src/src/ush_write.c \
../Core/Src/src/ush_write_utils.c 

OBJS += \
./Core/Src/src/ush.o \
./Core/Src/src/ush_autocomp.o \
./Core/Src/src/ush_autocomp_state.o \
./Core/Src/src/ush_autocomp_utils.o \
./Core/Src/src/ush_commands.o \
./Core/Src/src/ush_file.o \
./Core/Src/src/ush_node.o \
./Core/Src/src/ush_node_mount.o \
./Core/Src/src/ush_node_utils.o \
./Core/Src/src/ush_parse.o \
./Core/Src/src/ush_parse_char.o \
./Core/Src/src/ush_parse_utils.o \
./Core/Src/src/ush_process.o \
./Core/Src/src/ush_prompt.o \
./Core/Src/src/ush_read.o \
./Core/Src/src/ush_read_char.o \
./Core/Src/src/ush_read_utils.o \
./Core/Src/src/ush_reset.o \
./Core/Src/src/ush_utils.o \
./Core/Src/src/ush_write.o \
./Core/Src/src/ush_write_utils.o 

C_DEPS += \
./Core/Src/src/ush.d \
./Core/Src/src/ush_autocomp.d \
./Core/Src/src/ush_autocomp_state.d \
./Core/Src/src/ush_autocomp_utils.d \
./Core/Src/src/ush_commands.d \
./Core/Src/src/ush_file.d \
./Core/Src/src/ush_node.d \
./Core/Src/src/ush_node_mount.d \
./Core/Src/src/ush_node_utils.d \
./Core/Src/src/ush_parse.d \
./Core/Src/src/ush_parse_char.d \
./Core/Src/src/ush_parse_utils.d \
./Core/Src/src/ush_process.d \
./Core/Src/src/ush_prompt.d \
./Core/Src/src/ush_read.d \
./Core/Src/src/ush_read_char.d \
./Core/Src/src/ush_read_utils.d \
./Core/Src/src/ush_reset.d \
./Core/Src/src/ush_utils.d \
./Core/Src/src/ush_write.d \
./Core/Src/src/ush_write_utils.d 


# Each subdirectory must supply rules for building sources it contributes
Core/Src/src/%.o Core/Src/src/%.su: ../Core/Src/src/%.c Core/Src/src/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -DUSE_HAL_DRIVER -DSTM32F446xx -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -Os -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-Core-2f-Src-2f-src

clean-Core-2f-Src-2f-src:
	-$(RM) ./Core/Src/src/ush.d ./Core/Src/src/ush.o ./Core/Src/src/ush.su ./Core/Src/src/ush_autocomp.d ./Core/Src/src/ush_autocomp.o ./Core/Src/src/ush_autocomp.su ./Core/Src/src/ush_autocomp_state.d ./Core/Src/src/ush_autocomp_state.o ./Core/Src/src/ush_autocomp_state.su ./Core/Src/src/ush_autocomp_utils.d ./Core/Src/src/ush_autocomp_utils.o ./Core/Src/src/ush_autocomp_utils.su ./Core/Src/src/ush_commands.d ./Core/Src/src/ush_commands.o ./Core/Src/src/ush_commands.su ./Core/Src/src/ush_file.d ./Core/Src/src/ush_file.o ./Core/Src/src/ush_file.su ./Core/Src/src/ush_node.d ./Core/Src/src/ush_node.o ./Core/Src/src/ush_node.su ./Core/Src/src/ush_node_mount.d ./Core/Src/src/ush_node_mount.o ./Core/Src/src/ush_node_mount.su ./Core/Src/src/ush_node_utils.d ./Core/Src/src/ush_node_utils.o ./Core/Src/src/ush_node_utils.su ./Core/Src/src/ush_parse.d ./Core/Src/src/ush_parse.o ./Core/Src/src/ush_parse.su ./Core/Src/src/ush_parse_char.d ./Core/Src/src/ush_parse_char.o ./Core/Src/src/ush_parse_char.su ./Core/Src/src/ush_parse_utils.d ./Core/Src/src/ush_parse_utils.o ./Core/Src/src/ush_parse_utils.su ./Core/Src/src/ush_process.d ./Core/Src/src/ush_process.o ./Core/Src/src/ush_process.su ./Core/Src/src/ush_prompt.d ./Core/Src/src/ush_prompt.o ./Core/Src/src/ush_prompt.su ./Core/Src/src/ush_read.d ./Core/Src/src/ush_read.o ./Core/Src/src/ush_read.su ./Core/Src/src/ush_read_char.d ./Core/Src/src/ush_read_char.o ./Core/Src/src/ush_read_char.su ./Core/Src/src/ush_read_utils.d ./Core/Src/src/ush_read_utils.o ./Core/Src/src/ush_read_utils.su ./Core/Src/src/ush_reset.d ./Core/Src/src/ush_reset.o ./Core/Src/src/ush_reset.su ./Core/Src/src/ush_utils.d ./Core/Src/src/ush_utils.o ./Core/Src/src/ush_utils.su ./Core/Src/src/ush_write.d ./Core/Src/src/ush_write.o ./Core/Src/src/ush_write.su ./Core/Src/src/ush_write_utils.d ./Core/Src/src/ush_write_utils.o ./Core/Src/src/ush_write_utils.su

.PHONY: clean-Core-2f-Src-2f-src

