################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Core/Src/ush_src/commands/ush_cmd.c \
../Core/Src/ush_src/commands/ush_cmd_cat.c \
../Core/Src/ush_src/commands/ush_cmd_cd.c \
../Core/Src/ush_src/commands/ush_cmd_echo.c \
../Core/Src/ush_src/commands/ush_cmd_help.c \
../Core/Src/ush_src/commands/ush_cmd_ls.c \
../Core/Src/ush_src/commands/ush_cmd_mkdir.c \
../Core/Src/ush_src/commands/ush_cmd_mv.c \
../Core/Src/ush_src/commands/ush_cmd_pwd.c \
../Core/Src/ush_src/commands/ush_cmd_rm.c \
../Core/Src/ush_src/commands/ush_cmd_touch.c \
../Core/Src/ush_src/commands/ush_cmd_xxd.c 

OBJS += \
./Core/Src/ush_src/commands/ush_cmd.o \
./Core/Src/ush_src/commands/ush_cmd_cat.o \
./Core/Src/ush_src/commands/ush_cmd_cd.o \
./Core/Src/ush_src/commands/ush_cmd_echo.o \
./Core/Src/ush_src/commands/ush_cmd_help.o \
./Core/Src/ush_src/commands/ush_cmd_ls.o \
./Core/Src/ush_src/commands/ush_cmd_mkdir.o \
./Core/Src/ush_src/commands/ush_cmd_mv.o \
./Core/Src/ush_src/commands/ush_cmd_pwd.o \
./Core/Src/ush_src/commands/ush_cmd_rm.o \
./Core/Src/ush_src/commands/ush_cmd_touch.o \
./Core/Src/ush_src/commands/ush_cmd_xxd.o 

C_DEPS += \
./Core/Src/ush_src/commands/ush_cmd.d \
./Core/Src/ush_src/commands/ush_cmd_cat.d \
./Core/Src/ush_src/commands/ush_cmd_cd.d \
./Core/Src/ush_src/commands/ush_cmd_echo.d \
./Core/Src/ush_src/commands/ush_cmd_help.d \
./Core/Src/ush_src/commands/ush_cmd_ls.d \
./Core/Src/ush_src/commands/ush_cmd_mkdir.d \
./Core/Src/ush_src/commands/ush_cmd_mv.d \
./Core/Src/ush_src/commands/ush_cmd_pwd.d \
./Core/Src/ush_src/commands/ush_cmd_rm.d \
./Core/Src/ush_src/commands/ush_cmd_touch.d \
./Core/Src/ush_src/commands/ush_cmd_xxd.d 


# Each subdirectory must supply rules for building sources it contributes
Core/Src/ush_src/commands/%.o Core/Src/ush_src/commands/%.su: ../Core/Src/ush_src/commands/%.c Core/Src/ush_src/commands/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -DUSE_HAL_DRIVER -DSTM32F446xx -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -I../FATFS/Target -I../FATFS/App -I../Middlewares/Third_Party/FatFs/src -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F -Os -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-Core-2f-Src-2f-ush_src-2f-commands

clean-Core-2f-Src-2f-ush_src-2f-commands:
	-$(RM) ./Core/Src/ush_src/commands/ush_cmd.d ./Core/Src/ush_src/commands/ush_cmd.o ./Core/Src/ush_src/commands/ush_cmd.su ./Core/Src/ush_src/commands/ush_cmd_cat.d ./Core/Src/ush_src/commands/ush_cmd_cat.o ./Core/Src/ush_src/commands/ush_cmd_cat.su ./Core/Src/ush_src/commands/ush_cmd_cd.d ./Core/Src/ush_src/commands/ush_cmd_cd.o ./Core/Src/ush_src/commands/ush_cmd_cd.su ./Core/Src/ush_src/commands/ush_cmd_echo.d ./Core/Src/ush_src/commands/ush_cmd_echo.o ./Core/Src/ush_src/commands/ush_cmd_echo.su ./Core/Src/ush_src/commands/ush_cmd_help.d ./Core/Src/ush_src/commands/ush_cmd_help.o ./Core/Src/ush_src/commands/ush_cmd_help.su ./Core/Src/ush_src/commands/ush_cmd_ls.d ./Core/Src/ush_src/commands/ush_cmd_ls.o ./Core/Src/ush_src/commands/ush_cmd_ls.su ./Core/Src/ush_src/commands/ush_cmd_mkdir.d ./Core/Src/ush_src/commands/ush_cmd_mkdir.o ./Core/Src/ush_src/commands/ush_cmd_mkdir.su ./Core/Src/ush_src/commands/ush_cmd_mv.d ./Core/Src/ush_src/commands/ush_cmd_mv.o ./Core/Src/ush_src/commands/ush_cmd_mv.su ./Core/Src/ush_src/commands/ush_cmd_pwd.d ./Core/Src/ush_src/commands/ush_cmd_pwd.o ./Core/Src/ush_src/commands/ush_cmd_pwd.su ./Core/Src/ush_src/commands/ush_cmd_rm.d ./Core/Src/ush_src/commands/ush_cmd_rm.o ./Core/Src/ush_src/commands/ush_cmd_rm.su ./Core/Src/ush_src/commands/ush_cmd_touch.d ./Core/Src/ush_src/commands/ush_cmd_touch.o ./Core/Src/ush_src/commands/ush_cmd_touch.su ./Core/Src/ush_src/commands/ush_cmd_xxd.d ./Core/Src/ush_src/commands/ush_cmd_xxd.o ./Core/Src/ush_src/commands/ush_cmd_xxd.su

.PHONY: clean-Core-2f-Src-2f-ush_src-2f-commands

