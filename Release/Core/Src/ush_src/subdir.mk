################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Core/Src/ush_src/ush.c \
../Core/Src/ush_src/ush_commands.c \
../Core/Src/ush_src/ush_file.c \
../Core/Src/ush_src/ush_node.c \
../Core/Src/ush_src/ush_node_utils.c \
../Core/Src/ush_src/ush_parse.c \
../Core/Src/ush_src/ush_parse_char.c \
../Core/Src/ush_src/ush_parse_utils.c \
../Core/Src/ush_src/ush_process.c \
../Core/Src/ush_src/ush_prompt.c \
../Core/Src/ush_src/ush_read.c \
../Core/Src/ush_src/ush_read_char.c \
../Core/Src/ush_src/ush_read_utils.c \
../Core/Src/ush_src/ush_reset.c \
../Core/Src/ush_src/ush_utils.c \
../Core/Src/ush_src/ush_write.c \
../Core/Src/ush_src/ush_write_utils.c 

OBJS += \
./Core/Src/ush_src/ush.o \
./Core/Src/ush_src/ush_commands.o \
./Core/Src/ush_src/ush_file.o \
./Core/Src/ush_src/ush_node.o \
./Core/Src/ush_src/ush_node_utils.o \
./Core/Src/ush_src/ush_parse.o \
./Core/Src/ush_src/ush_parse_char.o \
./Core/Src/ush_src/ush_parse_utils.o \
./Core/Src/ush_src/ush_process.o \
./Core/Src/ush_src/ush_prompt.o \
./Core/Src/ush_src/ush_read.o \
./Core/Src/ush_src/ush_read_char.o \
./Core/Src/ush_src/ush_read_utils.o \
./Core/Src/ush_src/ush_reset.o \
./Core/Src/ush_src/ush_utils.o \
./Core/Src/ush_src/ush_write.o \
./Core/Src/ush_src/ush_write_utils.o 

C_DEPS += \
./Core/Src/ush_src/ush.d \
./Core/Src/ush_src/ush_commands.d \
./Core/Src/ush_src/ush_file.d \
./Core/Src/ush_src/ush_node.d \
./Core/Src/ush_src/ush_node_utils.d \
./Core/Src/ush_src/ush_parse.d \
./Core/Src/ush_src/ush_parse_char.d \
./Core/Src/ush_src/ush_parse_utils.d \
./Core/Src/ush_src/ush_process.d \
./Core/Src/ush_src/ush_prompt.d \
./Core/Src/ush_src/ush_read.d \
./Core/Src/ush_src/ush_read_char.d \
./Core/Src/ush_src/ush_read_utils.d \
./Core/Src/ush_src/ush_reset.d \
./Core/Src/ush_src/ush_utils.d \
./Core/Src/ush_src/ush_write.d \
./Core/Src/ush_src/ush_write_utils.d 


# Each subdirectory must supply rules for building sources it contributes
Core/Src/ush_src/%.o Core/Src/ush_src/%.su: ../Core/Src/ush_src/%.c Core/Src/ush_src/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -DUSE_HAL_DRIVER -DSTM32F446xx -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -I../FATFS/Target -I../FATFS/App -I../Middlewares/Third_Party/FatFs/src -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F -Os -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-Core-2f-Src-2f-ush_src

clean-Core-2f-Src-2f-ush_src:
	-$(RM) ./Core/Src/ush_src/ush.d ./Core/Src/ush_src/ush.o ./Core/Src/ush_src/ush.su ./Core/Src/ush_src/ush_commands.d ./Core/Src/ush_src/ush_commands.o ./Core/Src/ush_src/ush_commands.su ./Core/Src/ush_src/ush_file.d ./Core/Src/ush_src/ush_file.o ./Core/Src/ush_src/ush_file.su ./Core/Src/ush_src/ush_node.d ./Core/Src/ush_src/ush_node.o ./Core/Src/ush_src/ush_node.su ./Core/Src/ush_src/ush_node_utils.d ./Core/Src/ush_src/ush_node_utils.o ./Core/Src/ush_src/ush_node_utils.su ./Core/Src/ush_src/ush_parse.d ./Core/Src/ush_src/ush_parse.o ./Core/Src/ush_src/ush_parse.su ./Core/Src/ush_src/ush_parse_char.d ./Core/Src/ush_src/ush_parse_char.o ./Core/Src/ush_src/ush_parse_char.su ./Core/Src/ush_src/ush_parse_utils.d ./Core/Src/ush_src/ush_parse_utils.o ./Core/Src/ush_src/ush_parse_utils.su ./Core/Src/ush_src/ush_process.d ./Core/Src/ush_src/ush_process.o ./Core/Src/ush_src/ush_process.su ./Core/Src/ush_src/ush_prompt.d ./Core/Src/ush_src/ush_prompt.o ./Core/Src/ush_src/ush_prompt.su ./Core/Src/ush_src/ush_read.d ./Core/Src/ush_src/ush_read.o ./Core/Src/ush_src/ush_read.su ./Core/Src/ush_src/ush_read_char.d ./Core/Src/ush_src/ush_read_char.o ./Core/Src/ush_src/ush_read_char.su ./Core/Src/ush_src/ush_read_utils.d ./Core/Src/ush_src/ush_read_utils.o ./Core/Src/ush_src/ush_read_utils.su ./Core/Src/ush_src/ush_reset.d ./Core/Src/ush_src/ush_reset.o ./Core/Src/ush_src/ush_reset.su ./Core/Src/ush_src/ush_utils.d ./Core/Src/ush_src/ush_utils.o ./Core/Src/ush_src/ush_utils.su ./Core/Src/ush_src/ush_write.d ./Core/Src/ush_src/ush_write.o ./Core/Src/ush_src/ush_write.su ./Core/Src/ush_src/ush_write_utils.d ./Core/Src/ush_src/ush_write_utils.o ./Core/Src/ush_src/ush_write_utils.su

.PHONY: clean-Core-2f-Src-2f-ush_src

